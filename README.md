# nextra-logo-animation

[Nextra](https://nextra.site/) logo animation by [Shu Ding](https://github.com/shuding) for learning purposes.

## Development

Install [fnm](https://github.com/Schniz/fnm) (if necessary).

```bash
fnm install && fnm use && node --version && npm --version
```

```bash
npm install
```

```bash
npm run dev
```

```bash
npm run lint
```

```bash
npm run format
```

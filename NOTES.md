# Notes

- https://nextra.site/docs
- https://github.com/shuding/nextra/blob/nextra-theme-docs%402.13.3/docs/theme.config.tsx
- https://github.com/shuding/nextra/blob/main/LICENSE
- https://github.com/shuding/nextra/blob/nextra-theme-docs%402.13.3/packages/nextra-theme-docs/src/components/navbar.tsx#L97
- https://ishadeed.com/article/new-viewport-units/
- https://tailwindcss.com/docs/height#dynamic-viewport-height
- https://piccalil.li/blog/a-more-modern-css-reset/
- https://web.dev/articles/6-css-snippets-every-front-end-developer-should-know-in-2023#3_grid_pile

## Commands

```bash
npm install vue && npm install -D vite @vitejs/plugin-vue typescript vue-tsc create-vue-tsconfigs sort-package-json npm-run-all2 prettier
```

```bash
rm -rf node_modules/ && npm install
```
